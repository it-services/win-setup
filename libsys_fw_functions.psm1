﻿# Requisite IP address blocks
$tama = "198.82.152.183"
$imperial = "128.173.126.133"
$clientnet = "192.168.2.0/24"
$campusnet = "198.82.0.0/16","128.173.0.0/16","172.16.0.0/12","2001:468:c80::/48","2607:b400::/40"
$agileadminnet = "192.168.10.0/24"
$agiledatanet = "192.168.2.0/24"
$libnet = "172.20.4.0/22","128.173.124.0-128.173.127.255"

<#
 .Synopsis
 Sets default firewall settings.

 .Description
 This will enable all Windows Defender Firewall profiles with a basic policy of block all inbound and allow all outbound.
 This will also specify inbound rules for ICMP and RDP access.
#>
function Set-FirewallDefault {

    # Set default profile settings
    Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled True
    Set-NetFirewallProfile -DefaultInboundAction Block -DefaultOutboundAction Allow -NotifyOnListen False -AllowUnicastResponseToMulticast False

    # Clear all existing rules, then create standard rule set
    Remove-NetFirewallRule -All
    New-NetFirewallRule -DisplayName "Scripted - ICMPv4" -Direction Inbound -Action Allow -IcmpType Any -Protocol icmpv4
    New-NetFirewallRule -DisplayName "Scripted - ICMPv6" -Direction Inbound -Action Allow -IcmpType Any -Protocol icmpv6
    New-NetFirewallRule -DisplayName "Scripted - RDP Client Net" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 3389 -LocalAddress $clientnet -RemoteAddress $clientnet
    New-NetFirewallRule -DisplayName "Scripted - RDP Tama" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 3389 -RemoteAddress $tama

}

<#
 .Synopsis
 Sets Windows Firewall exceptions for common MDT services.

 .Description
 This will enable commmon exceptions for Microsoft Deployment Toolkit, including file sharing ports, SQL ports, and MDT monitoring service ports.
#>
function Set-FirewallMDTServices {
    New-NetFirewallRule -DisplayName "Scripted - Campus MDT SMB/NB TCP" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 139,445 -RemoteAddress $campusnet
    New-NetFirewallRule -DisplayName "Scripted - Campus MDT SMB/NB UDP" -Direction Inbound -Action Allow -Protocol UDP -LocalPort 137,138 -RemoteAddress $campusnet
    New-NetFirewallRule -DisplayName "Scripted - Campus MDT SQL TCP" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 1433 -RemoteAddress $campusnet
    New-NetFirewallRule -DisplayName "Scripted - Campus MDT SQL UDP" -Direction Inbound -Action Allow -Protocol UDP -LocalPort 1434 -RemoteAddress $campusnet
    New-NetFirewallRule -DisplayName "Scripted - Campus MDT Monitoring" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 9800,9801 -RemoteAddress $campusnet
}

<#
 .Synopsis
 Set Windows Firewall exception for RDP within the VT Blacksburg campus networks

 .Description
 This will enable TCP exceptions for Microsoft Remote Desktop Protocol within the Virginia Tech campus network
#>
function Set-FirewallRDPService {
    New-NetFirewallRule -DisplayName "Scripted - RDP Campus" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 3389 -RemoteAddress $campusnet
}

<#
 .Synopsis
 Sets Windows Firewall exceptions for SMB within the VT Blacksburg campus networks

 .Description
 This will enable common TCP and UDP exceptions for Server Message Block services within the Virginia Tech campus network
#>
function Set-FirewallSMBServices {
    New-NetFirewallRule -DisplayName "Scripted - Campus SMB/NB TCP" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 139,445 -RemoteAddress $campusnet
    New-NetFirewallRule -DisplayName "Scripted - Campus SMB/NB UDP" -Direction Inbound -Action Allow -Protocol UDP -LocalPort 137,138 -RemoteAddress $campusnet

}

<#
 .Synopsis
 Sets Windows Firewall exceptions for LPD within the VT Blacksburg campus networks

 .Description
 This will enable TCP exceptions for Line Printer Daemon service within the Virginia Tech campus network
#>
function Set-FirewallLPDService {
    New-NetFirewallRule -DisplayName "Scripted - Campus LPD" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 515 -RemoteAddress $campusnet

}

<#
 .Synopsis
 Set Windows Firewall exception for the Faronics Deep Freeze service

 .Description
 This will enable the Faronics Deep Freeze server service to listen on all TCP and UDP ports
#>
function Set-FirewallDeepFreezeService {
    New-NetFirewallRule -Displayname "Scripted - Deep Freeze Console TCP" -Direction Inbound -Action Allow -Protocol TCP -Program "C:\Program Files (x86)\Faronics\Deep Freeze Enterprise Server\DFServerService.exe"
    New-NetFirewallRule -Displayname "Scripted - Deep Freeze Console UDP" -Direction Inbound -Action Allow -Protocol UDP -Program "C:\Program Files (x86)\Faronics\Deep Freeze Enterprise Server\DFServerService.exe"
}

<#
 .Synopsis
 Set Windows Firewall exception for Microsoft SQL Server

 .Descrition
 This will enable Microsoft SQL server to listen to connections from the client network
#>
function Set-FirewallSQLClientNet {
    New-NetFirewallRule -DisplayName "Scripted - Client Net SQL" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 1433 -RemoteAddress $client
}

<#
 .Synopsis
 Set Windows Firewall excpetion for Web services

 .Description
 This will enable common HTTP and HTTPS ports globally
#>
function Set-FirewallWebService {
    New-NetFirewallRule -DisplayName "Scripted - HTTP" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 80
    New-NetFirewallRule -DisplayName "Scripted - HTTPS" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 443
}

<#
 .Synopsis
 Set Windows Firewall exceptions for common WSUS service ports

 .Description
 This will enable common ports HTTP and HTTPS ports for WSUS globally
#>
function Set-FirewallWSUSServices {
    New-NetFirewallRule -DisplayName "Scripted - WSUS" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 8530,8531
}

<#
 .Synopsis
 Set Windows Firewall default settings for Agisoft Cluster Server

 .Description
 This will set various default settings for Agisoft Cluster Server node, including RDP, SMB, and Agisoft Metashape server services
#>
function Set-FirewallAgileServerDefault {
    # Set default profile settings
    Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled True
    Set-NetFirewallProfile -DefaultInboundAction Block -DefaultOutboundAction Allow -NotifyOnListen False -AllowUnicastResponseToMulticast False

    # Clear all existing rules, then create standard rule set
    Remove-NetFirewallRule -All
    New-NetFirewallRule -DisplayName "Scripted - ICMPv4" -Direction Inbound -Action Allow -IcmpType Any -Protocol icmpv4
    New-NetFirewallRule -DisplayName "Scripted - ICMPv6" -Direction Inbound -Action Allow -IcmpType Any -Protocol icmpv6
    New-NetFirewallRule -DisplayName "Scripted - RDP Admin Net" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 3389 -LocalAddress $agileadminnet -RemoteAddress $agileadminnet
    New-NetFirewallRule -DisplayName "Scripted - LIB SMB/NB TCP" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 139,445 -RemoteAddress $campusnet
    New-NetFirewallRule -DisplayName "Scripted - LIB SMB/NB UDP" -Direction Inbound -Action Allow -Protocol UDP -LocalPort 137,138 -RemoteAddress $campusnet
    New-NetFirewallRule -DisplayName "Scripted - Data Net SMB/NB TCP" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 139,445 -LocalAddress $agiledatanet -RemoteAddress $agiledatanet
    New-NetFirewallRule -DisplayName "Scripted - Data Net SMB/NB UDP" -Direction Inbound -Action Allow -Protocol UDP -LocalPort 137,138 -LocalAddress $agiledatanet -RemoteAddress $agiledatanet
    New-NetFirewallRule -DisplayName "Scripted - Agisoft Client" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 5840 -RemoteAddress $campusnet
    New-NetFirewallRule -DisplayName "Scripted - Agisoft Node" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 5841 -LocalAddress $agiledatanet -RemoteAddress $agiledatanet
}

<#
 .Synopsis
 Set Windows Firewall default settings for Agisoft Cluster Node

 .Description
 This will set various default settings for Agisoft Cluster processing node, including RDP
#>
function Set-FirewallAgileNodeDefault {
    # Set default profile settings
    Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled True
    Set-NetFirewallProfile -DefaultInboundAction Block -DefaultOutboundAction Allow -NotifyOnListen False -AllowUnicastResponseToMulticast False

    # Clear all existing rules, then create standard rule set
    Remove-NetFirewallRule -All
    New-NetFirewallRule -DisplayName "Scripted - ICMPv4" -Direction Inbound -Action Allow -IcmpType Any -Protocol icmpv4
    New-NetFirewallRule -DisplayName "Scripted - ICMPv6" -Direction Inbound -Action Allow -IcmpType Any -Protocol icmpv6
    New-NetFirewallRule -DisplayName "Scripted - RDP Admin Net" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 3389 -LocalAddress $agileadminnet -RemoteAddress $agileadminnet
}
