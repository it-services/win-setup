 # Get module
$module = Invoke-WebRequest -Uri https://code.vt.edu/it-services/win-setup/raw/master/libsys_fw_functions.psm1?inline=false

# Install Directory
$installdir = $($Env:Programfiles + "\WindowsPowerShell\Modules\libsys_fw_functions")
$installfile = $installdir + "\libsys_fw_functions.psm1"

if (Test-Path $installdir) {
    Remove-Item -Recurse -Force $installdir
    New-Item -ItemType "directory" -Path $installdir
    New-Item -ItemType "file" -Path $installfile -Value $module.Content
} 
else {
    New-Item -ItemType "directory" -Path $installdir
    New-Item -ItemType "file" -Path $installfile -Value $module.Content
}

Import-Module -Name libsys_fw_functions 

